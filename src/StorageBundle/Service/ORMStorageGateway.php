<?php

namespace StorageBundle\Service;


use Doctrine\ORM\EntityManager;
use StorageBundle\Entity\Demo;

class ORMStorageGateway
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var EntityManager
     */
    private $mysqlEm;

    /**
     * @var EntityManager
     */
    private $sqliteEm;

    public function __construct(EntityManager $mysqlEm,EntityManager $sqliteEm)
    {
        $this->mysqlEm = $mysqlEm;
        $this->sqliteEm = $sqliteEm;
    }

    public function setDatabase($database){
        switch ($database) {
            case 'mysql': $this->em = $this->mysqlEm;break;
            case 'sqlite': $this->em = $this->sqliteEm;break;
        }
    }

    public function create()
    {
        $demo = new Demo();
        $demo->setText('lorem ipsum');
        $demo->setDate(new \DateTime());

        $this->em->persist($demo);
        $this->em->flush();
    }

    public function selectOneField($id){
        $repository = $this->em->getRepository('StorageBundle:Demo');
        $entity     = $repository->find($id);
        return $entity->getText();
    }

    public function selectOneRow($id){
        $repository = $this->em->getRepository('StorageBundle:Demo');
        return $repository->find($id);
    }

    public function selectEverything(){
        $repository = $this->em->getRepository('StorageBundle:Demo');
        return $repository->findAll();
    }

    public function update($id){
        $repository = $this->em->getRepository('StorageBundle:Demo');
        $entity = $repository->find($id);
        $entity->setText('lorem ipsum renew');

        $this->em->persist($entity);
        $this->em->flush();
    }

    public function delete($id){
        $repository = $this->em->getRepository('StorageBundle:Demo');
        $entity = $repository->find($id);
        $this->em->remove($entity);
        $this->em->flush();
    }
}