<?php

namespace StorageBundle\Service;

use Doctrine\DBAL\Driver\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class DBALStorageGateway
{
    /**
     * @var Connection
     */
    private $mysqlConnection;

    /**
     * @var Connection
     */
    private $sqliteConnection;

    /**
     * @var Connection
     */
    protected $connection;

    public function __construct(Connection $mysqlConnection, Connection $sqliteConnection){
        $this->mysqlConnection = $mysqlConnection;
        $this->sqliteConnection = $sqliteConnection;
    }

    public function setDatabaseConnection($database){
        switch ( $database ) {
            case 'mysql': $this->connection = $this->mysqlConnection;break;
            case 'sqlite': $this->connection = $this->sqliteConnection;break;
        }
    }

    public function create(){
        $date      = new \DateTime();
        $statement = <<<SQL
INSERT INTO demo (`text`,`date`) VALUES (:text,:datetime)
SQL;
        $preparedStatement = $this->connection->prepare($statement);
        $preparedStatement->bindValue('text', 'lorem ipsum');
        $preparedStatement->bindValue('datetime', $date->format('Y-m-d h:i:s'));
        $preparedStatement->execute();
    }

    public function selectOneField($id){
        $statement = <<<SQL
SELECT text FROM demo WHERE id = :id
SQL;
        $preparedStatement = $this->connection->prepare($statement);
        $preparedStatement->bindValue('id', $id);
        $preparedStatement->execute();
        return $preparedStatement->fetchColumn();
    }

    public function selectOneRow($id){
        $statement = <<<SQL
SELECT * FROM demo WHERE id = :id
SQL;
        $preparedStatement = $this->connection->prepare($statement);
        $preparedStatement->bindValue('id', $id);
        $preparedStatement->execute();
        return $preparedStatement->fetch();
    }

    public function selectEverything(){
        $statement = <<<SQL
SELECT * FROM demo
SQL;
        $preparedStatement = $this->connection->prepare($statement);
        $preparedStatement->execute();
        return $preparedStatement->fetchAll();
    }

    public function update($id){
        $statement = <<<SQL
UPDATE demo SET text = :text WHERE id = :id
SQL;
        $preparedStatement = $this->connection->prepare($statement);
        $preparedStatement->bindValue('id', $id);
        $preparedStatement->bindValue('text', 'lorem ipsum renew');
        $preparedStatement->execute();
    }

    public function delete($id){
        $statement = <<<SQL
DELETE FROM demo WHERE id = :id
SQL;
        $preparedStatement = $this->connection->prepare($statement);
        $preparedStatement->bindValue('id', $id);
        $preparedStatement->execute();
    }

    public function truncate(){
        $statement = <<<SQL
TRUNCATE TABLE demo
SQL;
        $preparedStatement = $this->mysqlConnection->prepare($statement);
        $preparedStatement->execute();
    }
}