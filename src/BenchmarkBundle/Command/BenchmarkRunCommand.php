<?php

namespace BenchmarkBundle\Command;


use StorageBundle\Service\DBALStorageGateway;
use StorageBundle\Service\ORMStorageGateway;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;

class BenchmarkRunCommand extends ContainerAwareCommand
{
    /**
     * @var bool
     */
    protected $verbose = false;

    /**
     * @var int
     */
    protected $entities = 1000;

    /**
     * @var string
     */
    protected $database = 'mysql';


    public function configure(){
        $this->setName('benchmark:run');
        $this->addOption('database', 'd', InputOption::VALUE_REQUIRED);
        $this->addOption('entities', 't', InputOption::VALUE_REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool|int|null
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->verbose  = $input->getOption('verbose')
            ? $output->setVerbosity(Output::VERBOSITY_VERBOSE)
            : $output->setVerbosity(Output::VERBOSITY_NORMAL);
        $this->entities = $input->getOption('entities') ? $input->getOption('entities') : 1000;
        $this->database = $input->getOption('database') ? $input->getOption('database') : 'mysql';

        $this->printSummary($this->runBenchmark($output), $output);
        return true;
    }

    protected function printSummary($summary, $output){
        $table = new Table($output);
        $table
            ->setHeaders(array('Create', 'SelectOneField', 'SelectOneRow', 'SelectEvery', 'Update', 'Delete', 'Total'))
            ->setRows(array(
                $summary['dbal'],
                $summary['orm']
            ))
        ;
        $table->render();
    }

    protected function runBenchmark(OutputInterface $output){
        $summary = [];
        $output->writeln('#############################################################################', OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('Create benchmark dbal:', OUTPUT::VERBOSITY_VERBOSE);
        $startTime      = microtime(true);
        $startTimeTotal = $startTime;
        $gateway        = $this->getDBALGateway();
        $gateway->setDatabaseConnection($this->database);
        for ($i = 0; $i < $this->entities; $i++) {
            $gateway->create();
        }
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['dbal'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Create run with dbal took %s s for %s entities', $result, $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('#############################################################################', OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('Select benchmark dbal:', OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        for ($i = 1; $i <= $this->entities; $i++) {
            $fields[] = $gateway->selectOneField($i);
        }
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['dbal'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Select run with dbal took %s s for %s entities to select one field', $result,
            $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        for ($i = 1; $i <= $this->entities; $i++) {
            $fields[] = $gateway->selectOneRow($i);
        }
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['dbal'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Select run with dbal took %s s for %s entities to select one row', $result,
            $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        $rows = $gateway->selectEverything();
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['dbal'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Select run with dbal took %s s for %s entities to select everything', $result,
            $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('#############################################################################', OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('Update benchmark dbal:', OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        for ($i = 1; $i < $this->entities; $i++) {
            $gateway->update($i);
        }
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['dbal'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Update run with dbal took %s s for %s entities', $result, $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('#############################################################################', OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('Delete benchmark dbal:', OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        for ($i = 1; $i < $this->entities; $i++) {
            $gateway->delete($i);
        }
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['dbal'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Delete run with dbal took %s s for %s entities', $result, $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('#############################################################################', OUTPUT::VERBOSITY_VERBOSE);
        $timeTotalDBAL = $endTime - $startTimeTotal;
        $gateway->truncate();
        $output->writeln('#############################################################################', OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('Create benchmark orm:', OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        $startTimeTotal = $startTime;
        $gateway = $this->getORMGateway();
        $gateway->setDatabase($this->database);
        for ($i = 0; $i < $this->entities; $i++) {
            $gateway->create();
        }
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['orm'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Create run with orm took %s s for %s entities', $result, $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('#############################################################################', OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('Select benchmark orm:', OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        for ($i = 1; $i <= $this->entities; $i++) {
            $fields[] = $gateway->selectOneField($i);
        }
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['orm'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Select run with orm took %s s for %s entities to select one field', $result,
            $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        for ($i = 1; $i <= $this->entities; $i++) {
            $fields[] = $gateway->selectOneRow($i);
        }
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['orm'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Select run with orm took %s s for %s entities to select one row', $result,
            $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        $rows = $gateway->selectEverything();
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['orm'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Select run with orm took %s s for %s entities to select everything', $result,
            $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('#############################################################################', OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('Update benchmark orm:', OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        for ($i = 1; $i <= $this->entities; $i++) {
            $gateway->update($i);
        }
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['orm'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Update run with orm took %s s for %s entities', $result, $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s ms per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('#############################################################################', OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('Delete benchmark orm:', OUTPUT::VERBOSITY_VERBOSE);
        $startTime = microtime(true);
        for ($i = 1; $i <= $this->entities; $i++) {
            $gateway->delete($i);
        }
        $endTime = microtime(true);
        $result = $endTime - $startTime;
        $summary['orm'][] = number_format(round($result / $this->entities, 6, PHP_ROUND_HALF_UP), 5);
        $output->writeln(sprintf('Delete run with orm took %s s for %s entities', $result, $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Average %s s per entity', $result / $this->entities), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln('#############################################################################', OUTPUT::VERBOSITY_VERBOSE);
        $timeTotalORM = $endTime - $startTimeTotal;
        $output->writeln(sprintf('Total runtime with dbal took %s s', $timeTotalDBAL), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Total runtime with orm took %s s', $timeTotalORM), OUTPUT::VERBOSITY_VERBOSE);
        $output->writeln(sprintf('Difference: %s s', $timeTotalORM-$timeTotalDBAL), OUTPUT::VERBOSITY_VERBOSE);
        $summary['dbal'][] = number_format(round($timeTotalDBAL, 6, PHP_ROUND_HALF_UP), 5);
        $summary['orm'][] = number_format(round($timeTotalORM, 6, PHP_ROUND_HALF_UP), 5);
        $this->getDBALGateway()->truncate();
        return $summary;
    }


    /**
     * @return DBALStorageGateway
     */
    protected function getDBALGateway(){
        return $this->getContainer()->get('storage.dbal.gateway');
    }

    /**
     * @return ORMStorageGateway
     */
    protected function getORMGateway(){
        return $this->getContainer()->get('storage.orm.gateway');
    }
}