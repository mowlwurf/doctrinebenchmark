doctrinebenchmark
=================

# Usage

Use the run command without arguments to run a crud (1000 entities) benchmark with pdo_mysql driver

```
bin/console bechmark:run
```

# Arguments

```
-v verbose
-d database
-e count of entities
```

## Verbose

Just print the information for each run, which you can find in the summary table, which is printed anyway

## Database

Until now there are two database types supported mysql & sqlite # default:mysql

## Count of entities

You are able to scale the benchmark to your needs, and set the count of entities which should be written, read, updated & deleted.